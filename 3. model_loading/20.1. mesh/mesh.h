#ifndef MESH_H
#define MESH_H

#include <string>
#include <vector>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "shaderClass.hpp"

struct Vertex {
  glm::vec3 Position;
  glm::vec3 Normal;
  glm::vec3 TexCoords;
};

struct Texture {
  unsigned int id;
  std::string type;
};

class Mesh {
public:
  Mesh();
  Mesh(const std::vector<Vertex> vertices,
       const std::vector<unsigned int> indices,
       const std::vector<Texture> textures);
  ~Mesh();

public:
  void Draw(Shader &shader);

public:
  // mesh data
  std::vector<Vertex> vertices;
  std::vector<unsigned int> indices;
  std::vector<Texture> textures;

private:
  void setupMesh();

private:
  // render data
  unsigned int VAO, VBO, EBO;
};

#endif // MESH_H
