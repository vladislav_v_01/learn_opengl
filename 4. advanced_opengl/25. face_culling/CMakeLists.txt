cmake_minimum_required(VERSION 3.19)

project(face_culling LANGUAGES C CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

find_package(glfw3 REQUIRED)
find_package(assimp REQUIRED)
#find_package(DL REQUIRED)
#find_package(pthread REQUIRED)
file(GLOB H_FILES ${CMAKE_CURRENT_SOURCE_DIR}/*.h
    ${CMAKE_CURRENT_SOURCE_DIR}/glad/*.h)
file(GLOB C_FILES ${CMAKE_CURRENT_SOURCE_DIR}/*.c
    ${CMAKE_CURRENT_SOURCE_DIR}/glad/*.c)
file(GLOB HPP_FILES ${CMAKE_CURRENT_SOURCE_DIR}/*.hpp)
file(GLOB CPP_FILES ${CMAKE_CURRENT_SOURCE_DIR}/*.cpp)
add_executable(${PROJECT_NAME} ${H_FILES} ${C_FILES} ${HPP_FILES} ${CPP_FILES})
target_link_libraries(${PROJECT_NAME}  ${OPENGL_glx_LIBRARY}  glfw assimp dl)
target_include_directories(${PROJECT_NAME} PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
target_include_directories(${PROJECT_NAME} PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
include_directories(${OPENGL_INCLUDE_DIR})
