#ifndef MODEL_H
#define MODEL_H

#include <string>
#include <vector>

#include <assimp/Importer.hpp>
#include <assimp/postprocess.h>
#include <assimp/scene.h>

#include "mesh.h"

#include "shader.h"

#include "stb_image.h"

unsigned int TextureFromFile(const char*        path,
                             const std::string& directory,
                             bool               gamma = false);

class Model
{
public:
    Model();
    Model(const char* path);

    ~Model();

public:
    void Draw(Shader& shader);

private:
    void                 loadModel(const std::string path);
    void                 processNode(aiNode* node, const aiScene* scene);
    Mesh                 processMesh(aiMesh* mesh, const aiScene* scene);
    std::vector<Texture> loadMaterialTextures(aiMaterial*   mat,
                                              aiTextureType type,
                                              std::string   typeName);

private:
    // model data
    std::vector<Mesh>    meshes;
    std::vector<Texture> textures_loaded;
    std::string          directory;
};

#endif // MODEL_H
