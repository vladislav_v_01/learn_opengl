#ifndef MESH_H
#define MESH_H

#include <string>
#include <vector>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "shaderClass.hpp"

struct Vertex
{
    // position
    glm::vec3 Position;
    // normal
    glm::vec3 Normal;
    // texCoords
    glm::vec2 TexCoords;
    // tangent
    glm::vec3 Tangent;
    // bitangent
    glm::vec3 Bitangent;
};

struct Texture
{
    unsigned int id;
    std::string  type;
    std::string  path;
};

class Mesh
{
public:
    Mesh();
    Mesh(const std::vector<Vertex>       vertices,
         const std::vector<unsigned int> indices,
         const std::vector<Texture>      textures);
    ~Mesh();

public:
    void Draw(Shader& shader);

public:
    // mesh data
    std::vector<Vertex>       vertices;
    std::vector<unsigned int> indices;
    std::vector<Texture>      textures;
    unsigned int              VAO;

private:
    void setupMesh();

private:
    // render data
    unsigned int VBO, EBO;
};

#endif // MESH_H
