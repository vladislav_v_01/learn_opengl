#ifndef SHADER_CLASS_HPP_
#define SHADER_CLASS_HPP_

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

#include "glad/glad.h"

class Shader
{
public:
    unsigned int ID;
    // конструктор считывает и создаёт шейдера из исходников
    Shader(const char *vertexPath, const char *fragmentPath);
    // активация шейдера
    void use();
    // методы для работы с юниформами
    void setBool(const std::string &name, bool value) const;
    void setInt(const std::string &name, int value) const;
    void setFloat(const std::string &name, float value) const;
};

#endif
