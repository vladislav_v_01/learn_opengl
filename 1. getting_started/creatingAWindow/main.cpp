// глад всегда нужно подключать раньше, чем любые библиотеки, использующие OpenGL!!!!
#include "glad/glad.h"

#include <GLFW/glfw3.h>
#include <iostream>

// Объявим функцию, которая будет вызываться при изменении размеров окна.
void frameBufferSizeCallBack(GLFWwindow *window, int width, int height);
// Создадим функцию, которая бует обрабатывать нажатие клавиши Esc.
void processInput(GLFWwindow *window)
{
    // Если клавиша Esc была нажата, то закрываем окно.
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, true);
    }
}
int main()
{
    // Инитиализируем glfw
    int result = glfwInit();
    if (result == GLFW_FALSE) {
        std::cout << "Failed to initialize GLFW library!" << std::endl;
        glfwTerminate();
        return -1;
    }
    // Конфигурируем glfw
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    // Следующий комментарий нужен только для Mak OS X.
    //    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    // Создаём окно при помощи glfwCreateWindow(). Параметрами указываем: длину, высоту, имя,
    // два последних оператора передаём как nullptr.
    GLFWwindow *window = glfwCreateWindow(800, 600, "LearnOpenGL", nullptr, nullptr);
    if (window == nullptr) {
        std::cout << "Failed to create GLFW widnow!" << std::endl;
        glfwTerminate();
        return -1;
    }
    // Говорим glfw сделать только что созданное окно основным контекстом в данном потоке.
    glfwMakeContextCurrent(window);

    if (!gladLoadGLLoader((GLADloadproc) glfwGetProcAddress)) {
        std::cout << "Failed to initialize GLAD!" << std::endl;
        return -1;
    }
    // Передадим значения длинны и высоты окна в OpenGL, для того, чтобы OpenGL знал,
    // где он может отображать данные.
    // Первые два параметра говорят о левом нижнем угле окна,
    // третий и чертвёртый -- длина и высота в пикселях соответсвенно.
    glViewport(0, 0, 800, 600);
    // Установим функцию, которая будет вызываться при изменении размеров окна.
    glfwSetFramebufferSizeCallback(window, frameBufferSizeCallBack);
    // Напишем бесконечный цикл, обрабатывающий собития. Пока он будет обрабатывать
    // только одно событие (закрытие окна).
    while (!glfwWindowShouldClose(window)) {
        // Вызываем функции обработки нажатия клавиш на клавиатуре.
        processInput(window);
        // Очистим наш экран в какой-нибудь цвет.
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);
        // Используя методику double buffer отрисовываем изображение.
        glfwSwapBuffers(window);
        // Все события (мышка, клавиатура и т. д.) обрабатываются данной функцией.
        // Так как мы не реализовали функции, для обработки каких либо событий,
        // то они будут просто игнорироваться.
        glfwPollEvents();
    }
    // Освобождаем все ресуры GLFW.
    glfwTerminate();
    return 0;
}

void frameBufferSizeCallBack(GLFWwindow *window, int width, int height)
{
    glViewport(0, 0, width, height);
}
